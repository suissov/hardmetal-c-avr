src = $(wildcard *.c)
obj = $(src:.c=.o)

test: $(obj)
	$(CC) -w -o $@ $^ -I.

.PHONY: clean
clean:
	rm -f $(obj) test
.PHONY: run
run:
	./test
