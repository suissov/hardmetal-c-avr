#include <avr/io.h> // Importeer info van 328p chip
#include <util/delay.h> // Importeer de delay functie

#define begin {   // "begin" is beter te lezen dan {
#define end {      

void main()
begin
  /* Initialiseren Data Direction Register D: */
  /* - maak output van Pin D2                 */
  /*        76543210          (posities bits) */  
  DDRD =  0b00000100;
  /* - zet output Pin D2  laag (0 V)          */
  /*        76543210                          */
  PORTD = 0b00000000; // Turn LED off
  
  /* Begin de oneindige programmaloop         */
  while(1)
  begin
    /* Zet output Port D2 hoog                */
    PORTD = 0b00000100;
    _delay_ms(200); // wacht 200 millisec.   
    PORTD = 0b00000000; 
    _delay_ms(200); // wacht 200 millisec.    
  end // einde loop
end //einde hoofprogramma (main)

